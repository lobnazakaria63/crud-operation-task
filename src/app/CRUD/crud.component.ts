import {Component ,OnInit,Directive, forwardRef, Attribute,OnChanges, SimpleChanges,Input} from '@angular/core';
import { CrudService } from "./crud.service";
import { ICustomers } from "./customers";
import { FormGroup } from "@angular/forms";
import { FormControl, Validators,NG_VALIDATORS,Validator,AbstractControl,ValidatorFn } from "@angular/forms";
import {CustomerSearchPipe} from './customer-search.pipe';
@Component({
    selector : 'crud-app',
    
    templateUrl : 'app/CRUD/crud.html',
    styleUrls :['app/CRUD/crud.css']
})
export class CrudOperation implements OnInit {
     
     customers:ICustomers[];
     showTableAll:boolean=false;
     showTableSingle:boolean=false;
     showAddForm:boolean=false;
     showSearchForm : boolean=false;
     showUpdateForm:boolean=true;
     showDeleteForm: boolean=false;
     showSearch: boolean=false;
     errorMessage: string;
     addForm:FormGroup;
     delForm:FormGroup;
     searchForm:FormGroup;
     updateForm:FormGroup;
     id :ICustomers;
     firstname :ICustomers;
     model :any={};
     model2:any={};
     myValue:number;
   //we injected the service inside the crud component, we use constructor to define dependency  
  constructor(private _crudService : CrudService ){}
addCust(){
    this.customers.push(this.model);
    
}

deleteCustomer(id:ICustomers, index:number)
{
    this._crudService.deleteCustomer(id)
    .subscribe(Response=>this.customers.splice(index,1));
}
updateCust(){
    let k = this.myValue;
    for (let i=0 ; i<this.customers.length ; i++)
        {
            if (i===k)
                {
                    this.customers[i]=this.model2;
                    
                }
        }
}


onAddSubmit({ value, valid }: { value: ICustomers, valid: boolean }) {
    console.log(value, valid);
    
    this._crudService.addCustomer(value) .subscribe(Response=>{});
    this.addCust();
    
   }

 /*onDelSubmit({ value, valid }: { value: ICustomers, valid: boolean }) {
    let cust = this.customers;
    console.log(value, valid);
    this._crudService.deleteCustomer(value).subscribe(Response=>{});
    this.deleteCust(value.id);
    }*/

onSearchSubmit({ value, valid }: { value: ICustomers, valid: boolean }) {
    console.log(value, valid);
    this._crudService.searchCustomer(value).subscribe(Response=>{});

    }

onUpdateSubmit({ value, valid }: { value: ICustomers, valid: boolean }) {
    console.log(value, valid);
    this._crudService.updateCustomer(value).subscribe(Response=>{});
    this.updateCust();
    }
   
  ngOnInit():void{
    this._crudService.getCustomers()
    .subscribe(customers=>this.customers=customers,
                error=>this.errorMessage=<any>error);

   

    this.addForm=new FormGroup({
        firstname : new FormControl('', Validators.required),
        lastname :new FormControl('', Validators.required),
        email :new FormControl('', Validators.required)
    });

    this.delForm=new FormGroup({
           id: new FormControl('',Validators.required)
    });
    this.searchForm=new FormGroup({
        firstname :new FormControl('',Validators.required)
    });

    this.updateForm=new FormGroup({
         id: new FormControl('',Validators.required),
         firstname : new FormControl('', Validators.required),
        lastname :new FormControl('', Validators.required),
        email: new FormControl('', Validators.required)
    });
     
  }

  


//testing email validation
 /*validateEmail(c: FormControl) {
  let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

  return EMAIL_REGEXP.test(c.value) ? null : {
    validateEmail: {
      valid: false
    }
  };
}*/



     toggleAddForm () :void
     {
         this.showAddForm=!this.showAddForm;
     }
        toggleSearchForm () :void
     {
         this.showSearchForm=!this.showSearchForm;
     }
        toggleUpdateForm (id:ICustomers,firstname:ICustomers,lastname:ICustomers,email:ICustomers, index: number ) :void
     {
        this.updateForm.patchValue({id : id,
             firstname:firstname ,
              lastname:lastname,
              email:email});
       
         this.myValue=index;
        
         
     }
        toggleDeleteForm () :void
     {
         this.showDeleteForm=true;
     }
        toggleShowTableAll():void
        {
           this.showTableAll=!this.showTableAll;
        }
         toggleShowTableSingle():void
        {
           this.showTableSingle=!this.showTableSingle;
        }
        toggleShowSearch():void
        {
           this.showSearch=!this.showSearch;
        }
}