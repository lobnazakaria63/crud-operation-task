import { PipeTransform, Pipe } from "@angular/core";
import {ICustomers} from "./customers";
@Pipe ({
    name:'customerSearch'
})
export class CustomerSearchPipe implements PipeTransform {
   transform (value :ICustomers[] , search : string) :ICustomers[] 
   {
       search = search ? search.toLocaleLowerCase() : null;
       return search ? value.filter((customer:ICustomers)=> 
       customer.firstname.toLocaleLowerCase().indexOf(search)!== -1): value;
   }
}