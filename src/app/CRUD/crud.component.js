"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var crud_service_1 = require("./crud.service");
var forms_1 = require("@angular/forms");
var forms_2 = require("@angular/forms");
var CrudOperation = (function () {
    //we injected the service inside the crud component, we use constructor to define dependency  
    function CrudOperation(_crudService) {
        this._crudService = _crudService;
        this.showTableAll = false;
        this.showTableSingle = false;
        this.showAddForm = false;
        this.showSearchForm = false;
        this.showUpdateForm = true;
        this.showDeleteForm = false;
        this.showSearch = false;
        this.model = {};
        this.model2 = {};
    }
    CrudOperation.prototype.addCust = function () {
        this.customers.push(this.model);
    };
    CrudOperation.prototype.deleteCustomer = function (id, index) {
        var _this = this;
        this._crudService.deleteCustomer(id)
            .subscribe(function (Response) { return _this.customers.splice(index, 1); });
    };
    CrudOperation.prototype.onAddSubmit = function (_a) {
        var value = _a.value, valid = _a.valid;
        console.log(value, valid);
        this._crudService.addCustomer(value).subscribe(function (Response) { });
        this.addCust();
    };
    /*onDelSubmit({ value, valid }: { value: ICustomers, valid: boolean }) {
       let cust = this.customers;
       console.log(value, valid);
       this._crudService.deleteCustomer(value).subscribe(Response=>{});
       this.deleteCust(value.id);
       }*/
    CrudOperation.prototype.onSearchSubmit = function (_a) {
        var value = _a.value, valid = _a.valid;
        console.log(value, valid);
        this._crudService.searchCustomer(value).subscribe(function (Response) { });
    };
    CrudOperation.prototype.onUpdateSubmit = function (_a) {
        var value = _a.value, valid = _a.valid;
        console.log(value, valid);
        this._crudService.updateCustomer(value).subscribe(function (Response) { });
    };
    CrudOperation.prototype.ngOnInit = function () {
        var _this = this;
        this._crudService.getCustomers()
            .subscribe(function (customers) { return _this.customers = customers; }, function (error) { return _this.errorMessage = error; });
        this.addForm = new forms_1.FormGroup({
            firstname: new forms_2.FormControl('', forms_2.Validators.required),
            lastname: new forms_2.FormControl('', forms_2.Validators.required),
            email: new forms_2.FormControl('', forms_2.Validators.required)
        });
        this.delForm = new forms_1.FormGroup({
            id: new forms_2.FormControl('', forms_2.Validators.required)
        });
        this.searchForm = new forms_1.FormGroup({
            firstname: new forms_2.FormControl('', forms_2.Validators.required)
        });
        this.updateForm = new forms_1.FormGroup({
            id: new forms_2.FormControl('', forms_2.Validators.required),
            firstname: new forms_2.FormControl('', forms_2.Validators.required),
            lastname: new forms_2.FormControl('', forms_2.Validators.required),
            email: new forms_2.FormControl('', forms_2.Validators.required)
        });
    };
    //testing email validation
    /*validateEmail(c: FormControl) {
     let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
   
     return EMAIL_REGEXP.test(c.value) ? null : {
       validateEmail: {
         valid: false
       }
     };
   }*/
    CrudOperation.prototype.toggleAddForm = function () {
        this.showAddForm = !this.showAddForm;
    };
    CrudOperation.prototype.toggleSearchForm = function () {
        this.showSearchForm = !this.showSearchForm;
    };
    CrudOperation.prototype.toggleUpdateForm = function (id, firstname, lastname, email, index) {
        this.updateForm.patchValue({ id: id,
            firstname: firstname,
            lastname: lastname,
            email: email });
    };
    CrudOperation.prototype.toggleDeleteForm = function () {
        this.showDeleteForm = true;
    };
    CrudOperation.prototype.toggleShowTableAll = function () {
        this.showTableAll = !this.showTableAll;
    };
    CrudOperation.prototype.toggleShowTableSingle = function () {
        this.showTableSingle = !this.showTableSingle;
    };
    CrudOperation.prototype.toggleShowSearch = function () {
        this.showSearch = !this.showSearch;
    };
    return CrudOperation;
}());
CrudOperation = __decorate([
    core_1.Component({
        selector: 'crud-app',
        templateUrl: 'app/CRUD/crud.html',
        styleUrls: ['app/CRUD/crud.css']
    }),
    __metadata("design:paramtypes", [crud_service_1.CrudService])
], CrudOperation);
exports.CrudOperation = CrudOperation;
//# sourceMappingURL=crud.component.js.map