import { Injectable } from "@angular/core";
import { ICustomers } from "./customers";
import { Http, Response, RequestOptions ,Headers} from "@angular/http";
import { Observable } from "rxjs/Observable";
import { URLSearchParams, RequestOptionsArgs, RequestMethod } from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
//import * as $ from 'jquery';

@Injectable()

export class CrudService {
  private _getCustomerUrl='http://localhost/showAll.php';
  private _postCustomerUrl='http://localhost/insert.php';
  private _deleteCustomerUrl='http://localhost/delete.php';
  private _searchCustomerUrl='http://localhost/search.php';
  private _updateCustomerUrl='http://localhost/update.php';


  constructor (private _http : Http){}


  //getting all data from the database in a table in the web application
   getCustomers():Observable<ICustomers[]>
   {
     return this._http.get(this._getCustomerUrl)
     .map((response:Response)=><ICustomers[]>response.json())
     .do(data => console.log('All: '+JSON.stringify(data)))
     .catch (this.handleError); 
   }

//.........................................................
   //adding data in the database method=> finished

   
  addCustomer(customer:ICustomers):Observable<ICustomers>
   {
     
    
     let urlSearchParams = new URLSearchParams();
    urlSearchParams.append('firstname', customer.firstname);
    urlSearchParams.append('lastname', customer.lastname);
    urlSearchParams.append('email', customer.email);
    
     let body = urlSearchParams.toString();
    
     let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let op = new RequestOptions({ headers: headers,
    body:body });
     return this._http.post(this._postCustomerUrl,body,op)
     .map((response:Response)=>{let customer = response; })
     .catch (err =>{return this.handleError(err)}); 
   }
//.....................................................
//delete a record in the database 
 deleteCustomer( id : ICustomers): Observable<ICustomers> {
     
  let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('id', id.toString());
        let body = urlSearchParams.toString();
    
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded' });
     let options = new RequestOptions({ headers: headers, body:body  });
    return this._http
              .post(this._deleteCustomerUrl ,body, options)
              .map((response:Response)=>{let id = response; })
              .catch(this.handleError);
  }
//.....

//..............................................................................
//search a record in the database
  searchCustomer(firstname: ICustomers): Observable<ICustomers> {
       
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('firstname', firstname.firstname);
        let body = urlSearchParams.toString();
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers : headers,body: body});
        //test
       return  this._http.post(this._searchCustomerUrl,body,options)
        .map((response:Response)=>{<ICustomers>response.json(); })
        .do(data => console.log('getCustomer: ' + JSON.stringify(data)))
            .catch(this.handleError);
       
    }
        //..........
        
//............................................................
//update a record in the database

 updateCustomer(customer: ICustomers ): Observable<ICustomers> {
       // const url = `${this.baseUrl}/${product.id}`;
    let urlSearchParams = new URLSearchParams();
      urlSearchParams.append('id', customer.id.toString());
      urlSearchParams.append('firstname', customer.firstname);
      urlSearchParams.append('lastname', customer.lastname);
      urlSearchParams.append('email', customer.email);
    
     let body = urlSearchParams.toString();
    
     let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
     let options = new RequestOptions({ headers : headers,body: body});
        return this._http.post(this._updateCustomerUrl, body, options)
            .map((response:Response)=>{let customer = response; })
            .do(data => console.log('updateProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

//..............................................................................
    private handleError(error:Response)
    {
      console.error(error);
      return Observable.throw(error.json().error || 'server error');
    }
}