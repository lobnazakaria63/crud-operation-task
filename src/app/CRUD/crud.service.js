"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var http_2 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/do");
//import * as $ from 'jquery';
var CrudService = (function () {
    function CrudService(_http) {
        this._http = _http;
        this._getCustomerUrl = 'http://localhost/showAll.php';
        this._postCustomerUrl = 'http://localhost/insert.php';
        this._deleteCustomerUrl = 'http://localhost/delete.php';
        this._searchCustomerUrl = 'http://localhost/search.php';
        this._updateCustomerUrl = 'http://localhost/update.php';
    }
    //getting all data from the database in a table in the web application
    CrudService.prototype.getCustomers = function () {
        return this._http.get(this._getCustomerUrl)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    //.........................................................
    //adding data in the database method=> finished
    CrudService.prototype.addCustomer = function (customer) {
        var _this = this;
        var urlSearchParams = new http_2.URLSearchParams();
        urlSearchParams.append('firstname', customer.firstname);
        urlSearchParams.append('lastname', customer.lastname);
        urlSearchParams.append('email', customer.email);
        var body = urlSearchParams.toString();
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var op = new http_1.RequestOptions({ headers: headers,
            body: body });
        return this._http.post(this._postCustomerUrl, body, op)
            .map(function (response) { var customer = response; })
            .catch(function (err) { return _this.handleError(err); });
    };
    //.....................................................
    //delete a record in the database 
    CrudService.prototype.deleteCustomer = function (id) {
        var urlSearchParams = new http_2.URLSearchParams();
        urlSearchParams.append('id', id.toString());
        var body = urlSearchParams.toString();
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers, body: body });
        return this._http
            .post(this._deleteCustomerUrl, body, options)
            .map(function (response) { var id = response; })
            .catch(this.handleError);
    };
    //.....
    //..............................................................................
    //search a record in the database
    CrudService.prototype.searchCustomer = function (firstname) {
        var urlSearchParams = new http_2.URLSearchParams();
        urlSearchParams.append('firstname', firstname.firstname);
        var body = urlSearchParams.toString();
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers, body: body });
        //test
        return this._http.post(this._searchCustomerUrl, body, options)
            .map(function (response) { response.json(); })
            .do(function (data) { return console.log('getCustomer: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    //..........
    //............................................................
    //update a record in the database
    CrudService.prototype.updateCustomer = function (customer) {
        // const url = `${this.baseUrl}/${product.id}`;
        var urlSearchParams = new http_2.URLSearchParams();
        urlSearchParams.append('id', customer.id.toString());
        urlSearchParams.append('firstname', customer.firstname);
        urlSearchParams.append('lastname', customer.lastname);
        urlSearchParams.append('email', customer.email);
        var body = urlSearchParams.toString();
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers, body: body });
        return this._http.post(this._updateCustomerUrl, body, options)
            .map(function (response) { var customer = response; })
            .do(function (data) { return console.log('updateProduct: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    //..............................................................................
    CrudService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'server error');
    };
    return CrudService;
}());
CrudService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CrudService);
exports.CrudService = CrudService;
//# sourceMappingURL=crud.service.js.map