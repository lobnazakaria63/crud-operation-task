export interface ICustomers{
    id:number;
    firstname:string;
    lastname:string;
    email:string;
    reg_date:string;
}