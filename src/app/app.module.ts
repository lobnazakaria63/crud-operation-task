import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { CrudOperation } from "./CRUD/crud.component";
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';
import { CustomerSearchPipe } from "./CRUD/customer-search.pipe";


@NgModule({
  imports:      [ BrowserModule, HttpModule,FormsModule,ReactiveFormsModule ],
  declarations: [ CrudOperation, AppComponent,CustomerSearchPipe],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
