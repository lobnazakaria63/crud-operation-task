import { Component } from '@angular/core';
import { CrudOperation } from "./CRUD/crud.component";
import {CrudService} from "./CRUD/crud.service";
@Component({
  selector: 'my-app',
  template: `<div class='text-center text-primary'><h1 > {{name}}</h1></div>
               <hr>
                  <div><crud-app class='text-primary'></crud-app></div>`,
  styles:['hr{width:50%;border-color:silver}'],
  providers:[CrudService]
})
export class AppComponent  { name : string = 'CRUD OPERATION'; }
